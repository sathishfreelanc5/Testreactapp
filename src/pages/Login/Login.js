import React from 'react';
import './assets/css/login.css';
import './assets/images/bg.jpg';
class Login extends React.Component
{
   render(){
    return(
     <>
    <section id='bg'>
        <div className='container'>
            <div className='row'>
                <div className='col-md-3'>&nbsp;</div>
                <div className='col-md-6 py-5 px-5'>
            <div id="form-bg">
                <form className='form-bg'>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Email Address</label>
                        <input type='email' className="form-control" id="email" placeholder="Email Address"></input>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Password</label>
                        <input type='password' className="form-control" id="password" placeholder="Password"></input>
                    </div>
                    <button type='submit' className='btn btn-success'>Submit</button>            
                </form>
            </div>
                </div>
                <div className='col-md-3'>&nbsp;</div>
            </div>
        </div>
    </section>
    </>
    );
   }
}

export default Login;